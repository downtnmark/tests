package emmerich.code;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

public class Part1 {

	static final String dataURL = "https://gist.githubusercontent.com/jed204/92f90060d0fabf65792d6d479c45f31c/raw/346c44a23762749ede009623db37f4263e94ef2a/java2.json";
	
	ObjectMapper mapper = new ObjectMapper( );
	
	public static class Result {
		public int recv;
		public int sent;
		public Result( ) { this(0, 0); }
		public Result(int dataRecv, int dataSent) {
			this.recv = dataRecv;
			this.sent = dataSent;
		}
	}

	/**
	 * Process a JSON node item, looking (recursively) for the fields "recv" 
	 * and "sent".  If found, tally their values in the "soFar" data.  
	 * Otherwise, keep digging for these fields.  Return the amount tallied 
	 * so far.
	 */
	Result processNode(JsonNode node, Result soFar) {
		JsonNode recv = node.get("recv");
		JsonNode sent = node.get("sent");
		boolean recurse = true;
		if (recv != null) {
			recurse = false;
			soFar.recv += recv.asInt( );
		}
		if (sent != null) {
			recurse = false;
			soFar.sent += sent.asInt( );
		}
		if (recurse) {
			// Iterate over each sub-field and process its fields
			Iterator<Map.Entry<String, JsonNode>> fields = node.fields( );
			Map.Entry<String, JsonNode> item;
			for (item = fields.next( ); fields.hasNext( ); item = fields.next( )) {
				if (item.getValue( ) != null)
					soFar = processNode(item.getValue( ), soFar);
			}
		}
		
		return (soFar);
	}
	
	/**
	 * Convert the given JSON text into JSON node objects and begin processing 
	 * at the root.  Return the result structure with the tallies of "recv" 
	 * and "sent" fields found.
	 */
	public Result computeDataInfo(String jsonText) {
		Result totals = new Result( );
		try {
			JsonNode root = mapper.readTree(jsonText);
			totals = processNode(root, totals);
		} catch (Exception ex) {
			
		}
		
		return (totals);
	}
	
	public static void main(String[] args) {
		HttpHelper.Response json = null; 
		try {
			System.out.println("Processing input from URL: " + dataURL);
			json = HttpHelper.issueGetRequest(dataURL);
		} catch (Exception ex) {
			System.out.println("There was a problem reading data from the URL " + dataURL + "\n  " + ex.getMessage( ));
			return;
		}
		if (json.status != 200) {
			System.out.println("Could not retrieve data from the URL " + dataURL + " (HTTP status " + json.status + ")");
			return;
		}
		Part1 processor = new Part1( );
		Result totals = processor.computeDataInfo(json.contents);
		System.out.println("Totals:  recv=" + totals.recv + ", sent=" + totals.sent);
	}

}
