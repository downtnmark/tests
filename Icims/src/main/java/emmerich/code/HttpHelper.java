package emmerich.code;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class HttpHelper {

	public static class Response {
		public int status;
		public String contents;
		public Response( ) { }
		public Response(int respStatus, String respContents) {
			this.status = respStatus;
			this.contents = respContents;
		}
	}
	
	/**
	 * Issue a HTTP GET request using the indicated URL.  Return a result 
	 * structure with the status and contents (when the status is 200).
	 */
	public static Response issueGetRequest(String url) throws IOException {
		int status = 500;
		String contents = null;
		URL reqUrl = new URL(url);
		HttpURLConnection client = (HttpURLConnection) reqUrl.openConnection( );
		
		// Prepare and send the request to the indicated URL
		client.setRequestMethod("GET");
		client.setRequestProperty("Content-Type", "application/json");
		status = client.getResponseCode( );
		if (status == 200) {
			BufferedReader input = null;
			StringBuffer contentBuf = new StringBuffer( );
			try {
				String line;
				// The requested operation succeeded, so get the contents of the response
				input = new BufferedReader(new InputStreamReader(client.getInputStream( )));
				while ((line = input.readLine( )) != null) {
				    contentBuf.append(line);
				}
			} finally {
				if (input != null) input.close( );
			}
			contents = contentBuf.toString( );
		}
		
		return (new Response(status, contents));
	}
	
	public static void main(String[ ] args) {
		String testUrl = "https://www.google.com";
		try {
			HttpHelper.Response resp = HttpHelper.issueGetRequest(testUrl);
			System.out.println("Request to " + testUrl + " returned status: " + resp.status);
			if (resp.status == 200) {
				System.out.println("Response data:\n" + resp.contents.substring(0, 200));
			}
		} catch (Exception ex) {
			System.out.println("Get operation to " + testUrl + " failed: " + ex.getMessage( ));
		}
	}
}
