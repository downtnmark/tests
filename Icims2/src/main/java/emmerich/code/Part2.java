package emmerich.code;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.xml.bind.DatatypeConverter;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

public class Part2 {

	static final String dataURL = "https://gist.githubusercontent.com/anonymous/8f60e8f49158efdd2e8fed6fa97373a4/raw/01add7ea44ed12f5d90180dc1367915af331492e/java-data2.json";
	static final int WORKER_POOL_SIZE = 12;
	
	ObjectMapper mapper = new ObjectMapper( );
	
	public static class Result {
		public String uid;
		public String md5;
		public int index;
		public Result( ) { this(0, "", ""); }
		public Result(int index, String uid, String md5) {
			this.index = index;
			this.uid = uid;
			this.md5 = md5;
		}
	}
	
	/**
	 * This class implements the worker code that will be dispatched to threads 
	 * in the worker pool.
	 */
	public static class MD5Worker implements Runnable {

		JsonNode node;
		
		public MD5Worker(JsonNode workNode) {
			this.node = workNode;
		}
		
		/**
		 * Compute the MD5 hash for he given string.  Return the hash as a 
		 * hexidecimal text string of the hash's bytes.
		 */
		static String computeMD5(String text) throws NoSuchAlgorithmException {
			String md5Value = "";
			
			MessageDigest md = MessageDigest.getInstance("MD5");
			md.update(text.getBytes( ));
		    byte[] digest = md.digest( );
		    md5Value = DatatypeConverter.printHexBinary(digest);
			
			return (md5Value);
		}
		
		/**
		 * Using the JSON node provided during construction, get the "uid" 
		 * (and "index") fields from the JSON node, then compute the MD5 hash 
		 * for the "uid" field.  Display the results.
		 */
		@Override
		public void run( ) {
			try {
				JsonNode index = node.get("index");
				JsonNode uid = node.get("uid");
				Result result = new Result( );
				
				result.index = index.asInt( );
				result.uid = uid.asText( );
				result.md5 = computeMD5(result.uid);
				announce(result);
			} catch (Exception ex) {
				System.out.println("Unable to process item on thread " + Thread.currentThread().getId( ) + ": " + ex.getMessage( ));
			}
		}
		
		static synchronized void announce(Result item) {
			System.out.println("[Thread-" + Thread.currentThread( ).getId( ) + "] index=" + item.index + ", MD5=" + item.md5);
		}
	}
	
	/**
	 * Parse the given JSON text, and then iterate over the nodes found in 
	 * the JSON array "items".  Dispatch each item found to a thread pool for 
	 * processing.
	 */
	public void computeDataInfo(String jsonText) {

		try {
			JsonNode root = mapper.readTree(jsonText);
			JsonNode items = root.get("items");
			if (items != null) {
				Iterator<JsonNode> elements = items.elements( );
				ExecutorService workers = Executors.newFixedThreadPool(WORKER_POOL_SIZE);
				JsonNode item;
				for (item = elements.next( ); elements.hasNext( ); item = elements.next( )) {
					workers.execute(new MD5Worker(item));
				}
				workers.shutdown( );
			}
		} catch (Exception ex) {
			System.out.println("Unable to complete work for MD5 computations: " + ex.getMessage( ));
		}
	}
	
	public static void main(String[] args) {
		HttpHelper.Response json = null; 
		try {
			System.out.println("Processing input from URL: " + dataURL);
			json = HttpHelper.issueGetRequest(dataURL);
		} catch (Exception ex) {
			System.out.println("There was a problem reading data from the URL " + dataURL + "\n  " + ex.getMessage( ));
			return;
		}
		if (json.status != 200) {
			System.out.println("Could not retrieve data from the URL " + dataURL + " (HTTP status " + json.status + ")");
			return;
		}
		Part2 processor = new Part2( );
		processor.computeDataInfo(json.contents);
		System.out.println("Done.");
	}

}
